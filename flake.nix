{
  description = "Rename branches locally, as well as on the origin";

  outputs = { self, nixpkgs }:
    with import nixpkgs { system = "x86_64-linux"; }; {
      defaultPackage.x86_64-linux = pkgs.writeShellApplication {
        name = "git-rename-branch";

        runtimeInputs = with pkgs; [ git jq curl ];

        text = ''
          #!${stdenv.shell}
          ${builtins.readFile ./git-rename-branch.sh}
        '';

        checkPhase = "${stdenv.shellDryRun} $target";
      };

      app.x86_64-linux = {
        type = "app";
        program = "${self.defaultPackages.x86_64-linux}/bin/git-rename-branch";
      };

      devShell.x86_64-linux = pkgs.mkShell {
        buildInputs = with pkgs; [
          bash jq curl
        ];
      };
    };
}
