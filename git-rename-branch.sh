#!/bin/bash

usage() {
    echo "Usage: git-rename-branch [flags] <old> <new>"
    echo ""
    echo "Renames the branch 'old' to 'new' locally as well as on the remote."
    echo "Also automatically changes the default branch if necesarry."
    echo ""
    echo "Flags:"
    echo ""
    echo "    --gitlab-token \"\$GITLAB_TOKEN\""
    echo "        Personal access token for the Gitlab API"
    echo ""
    echo "    --github-token \"\$GITHUB_TOKEN\""
    echo "        Access token for the Github API"
}

github_token="${GITHUB_TOKEN:-}"
gitlab_token="${GITLAB_TOKEN:-}"
remote=origin

args=()
while [[ $# -gt 0 ]]; do
    case $1 in
        --github-token)
            github_token=$2
            shift
            shift
            ;;
        --gitlab-token)
            gitlab_token=$2
            shift
            shift
            ;;
        --remote)
            remote=$2
            shift
            shift
            ;;
        -h|--help)
            usage
            exit 1
            ;;
        -*|--*)
            echo "Unknown command-line argument: $1"
            usage
            exit 1
            ;;
        *)
            args+=("$1")
            shift
            ;;
    esac
done

if [ ${#args[@]} -ne 2 ]; then
    usage
    exit 1
fi

old="${args[0]:-}"
new="${args[1]:-}"

if ! $(git show-ref --heads --quiet "$old"); then
    echo "ERROR: Branch '$old' does not exist"
    usage
    exit 1
fi
if $(git show-ref --heads --quiet "$new"); then
    echo "ERROR: Branch '$new' already exists"
    usage
    exit 1
fi

url=$(git remote get-url "$remote")

if [[ $url =~ ^git@gitlab\.com:(.+)/(.+)\.git$ ]]
then
    service="gitlab"
    user="${BASH_REMATCH[1]}"
    project="${BASH_REMATCH[2]}"
elif [[ $url =~ ^https://gitlab\.com/(.+)/(.+).git$ ]]
then
    service="gitlab"
    user="${BASH_REMATCH[1]}"
    project="${BASH_REMATCH[2]}"
elif [[ $url =~ ^git@github\.com:(.+)/(.+)\.git$ ]]
then
    service="github"
    user="${BASH_REMATCH[1]}"
    project="${BASH_REMATCH[2]}"
elif [[ $url =~ ^https://github\.com/(.+)/(.+).git$ ]]
then
    service="github"
    user="${BASH_REMATCH[1]}"
    project="${BASH_REMATCH[2]}"
else
    echo "ERROR: Could not detect remote repository!"
    echo "$remote: $url"
    exit 1
fi

if [ $service = "gitlab" ] && [ -z "$gitlab_token" ]; then
    echo "ERROR: No Gitlab access token given"
    exit 1
fi
if [ $service = "github" ] && [ -z "$github_token" ]; then
    echo "Error: No Github access token given"
    exit 1
fi

# Make sure we are up to date first
git fetch "$remote" "$old:$old" --update-head-ok

# Absolutely make sure we are up-to-date!
ref=$(git show-ref --hash "refs/heads/$old")
remote_ref=$(git show-ref --hash "refs/remotes/$remote/$old")
if [ "$ref" != "$remote_ref" ]; then
    echo "ERROR: it looks like $old is not on the same commit as $remote/$old! Please merge the changes first!"
    exit 1
fi

if [ $service = "gitlab" ]; then
    # find the project in the API.
    # We do this early such that we fail if we cannot find the project.
    # We just assign the project_res variable here, which can be used later.
    project_res=$(curl -s --show-error -H "Authorization: Bearer $gitlab_token" "https://gitlab.com/api/v4/projects?archived=false&owned=true&search=$project" | jq -c ".[] | select(.path == \"$project\")")
    # project_res therefore has to be set for anything to make sense.
    if [ -z "$project_res" ]; then
        echo "ERROR: Project $project not found using the GitLab API!"
        exit 1
    fi
elif [ $service = "github" ]; then
    echo "renaming branch $old to $new..."
    # for github, we can directly rename the branch using the API!
    curl -s --show-error -o /dev/null \
        --request POST -H "Authorization: Bearer $github_token" \
        --url "https://api.github.com/repos/$user/$project/branches/$old/rename" \
        --data "{\"new_name\":\"$new\"}"
fi

#rename locally
git branch -m "$old" "$new"

# push renamed branch to remote
git push -u "$remote" "$new"

# gitlab needs a cleanup step after we pushed the new branch,
# because it can't directly rename a branch in the API or UI or anywhere
if [ $service = "gitlab" ]; then
    project_id=$(jq -r '.id' <<< $project_res)

    # Check if old is the default branch, update the project if that is the case
    default_branch=$(jq -r '.default_branch' <<< $project_res)
    if [ "$default_branch" = "$old" ]; then
        echo "$old is the default branch, change it to $new..."

        # make the new branch the default branch using the API
        curl -s --show-error -o /dev/null \
            --request PUT -H "Authorization: Bearer $gitlab_token" \
            --url "https://gitlab.com/api/v4/projects/$project_id" \
            --data "default_branch=$new"
    fi

    # Check if old has protected_branches settings, copy them for new and delete
    # the old ones if we find something
    protected_branches_res=$(curl -s --show-error -H "Authorization: Bearer $gitlab_token" "https://gitlab.com/api/v4/projects/$project_id/protected_branches/$old")

    if [ "$protected_branches_res" != "" ]; then
        echo "$old was protected, create a new rule for $new..."

        curl -s --show-error -o /dev/null \
            --request DELETE -H "Authorization: Bearer $gitlab_token" \
            --url "https://gitlab.com/api/v4/projects/$project_id/protected_branches/$old"

        protected_branches_req=$(jq -c ". | del(.id) | .name = \"$new\"" <<< $protected_branches_res)

        curl -s --show-error -o /dev/null \
            --request POST -H "Authorization: Bearer $gitlab_token" \
            --data "$protected_branches_req" \
            --url "https://gitlab.com/api/v4/projects/$project_id/protected_branches"
    fi
fi

if [ $service != "github" ]; then
    # delete remote branch - we don't need to do this for github, since
    # renaming the branch using the API already deleted the old one.
    git push "$remote" --delete "$old"
fi

# update HEAD, this is something githubs tutorial does.
git remote set-head "$remote" -a

# remove tracking references to the old branch
git remote prune "$remote"
