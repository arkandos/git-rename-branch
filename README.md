# git-rename-branch

In June 2020, the [Software Freedom Conservancy](https://sfconservancy.org/news/2020/jun/23/gitbranchname/) recognized that the default branch name `master` in git might be offensive to some people.

The script in this repository was made in my (ongoing) effort to be more inclusive in my own projects.

Please keep in mind that I developed it exactly for that purpose, so be careful and make backups if you're planning to use it for anything else.

I have used it successfully for both Github and Gitlab so far.

## What does it do?

Unfortunately, changing the default branch in Gitlab still is unnecessarily hard:

* You have to make a copy of the old default branch and push that
* Copy branch protection rules to that new branch in Gitlab
* Change the default branch of the repository in Gitlab
* After that, the old default branch can finally be deleted

(Compare that to Github, which has a deticated [_"rename branch"_ button](https://docs.github.com/en/repositories/configuring-branches-and-merges-in-your-repository/managing-branches-in-your-repository/renaming-a-branch) that works with the default branch as well!)

Of course, you have to update all local versions of the repository as well, which is simpler, but still kind of a hassle:

```bash
git branch -m $old $new # rename the branch locally
git branch -u origin/$new $new # setup tracking correctly
git remote prune origin # remove the old remote branch ref
```

The script in this repository automates all these steps.


## Installation

Just download the file :)

```bash
curl https://gitlab.com/arkandos/git-rename-branch/-/raw/main/git-rename-branch.sh > git-rename-branch.sh
chmod +x git-rename-branch.sh
```

To function correctly, you need to have the following dependencies installed:

``` bash
jq # A command-line tool for working with JSON
curl # To make those JSON API requests
git # Obviously?
```

If you're using Nix, you can also directly build the flake in this repository.

## Usage

```bash
git-rename-branch $old $new
# for example:
git-rename-branch master main
```

| Flag | Env | Default | Description |
|------|-----|---------|-------------|
| `--gitlab-token` | `$GITLAB_TOKEN` | `""` | A [personal access token](https://gitlab.com/-/profile/personal_access_tokens) to use with the Gitlab API |
| `--github-token` | `$GITHUB_TOKEN` | `""` | A [personal access token](https://github.com/settings/tokens) to use with the Github API. I only have tried the classic tokens, which need the `repo` permission. |
| `--remote` | | `origin` | The name of the remote of the Gitlab/Github server

